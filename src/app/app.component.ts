import {Component, OnInit} from '@angular/core';
import {combineLatest, forkJoin, Observable, throwError} from 'rxjs';
import {OfferService} from './service/offer.service';
import {delay, map, retryWhen, share, switchMap, take} from 'rxjs/operators';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  data$: Observable<any>;
  products$: Observable<any>;
  offers$: Observable<any>;

  constructor(private offerService: OfferService) {

  }

  ngOnInit(): void {
    this.data$ = this.offerService.all('co.uk', 'B07FP6QNQ7', 1, {}).pipe(
      retryWhen(errors => errors.pipe(
        delay(10000),
        take(3),
      )),
      share()
    );
    this.products$ = this.data$.pipe(
      switchMap(
        data => forkJoin(data.offers.map(offer => this.offerService.product(offer.offeringId, offer.asin, 'co.uk', data.sessionID).pipe(
          map((content: { offerID: string, stockQuantity: number, limit: number }) => {
            if (!content.stockQuantity) {
              return throwError('Error!');
            }

            return content;
          }),
          retryWhen(errors => errors.pipe(
            delay(10000),
            take(3),
          ))
        )))
      )
    );
    this.offers$ = combineLatest([this.data$, this.products$]).pipe(
      map(([data, products]) => {
        return data.offers.map(offer => {
          const result = products.find(product => product.offerID === offer.offeringId);

          if (result) {
            return {...offer, stockQuantity: result.stockQuantity, limit: result.limit};
          }

          return offer;
        });
      })
    );
  }
}
