import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class OfferService {

  constructor(private http: HttpClient) {
  }

  all(marketplace, asin, page, conditions) {
    const formData = new FormData();
    formData.append('marketplace', marketplace);
    formData.append('asin', asin);
    formData.append('page', page);


    return this.http.post('http://localhost:3002/index.php', formData);
  }

  product(offerID, asin, marketplace, sessionID) {
    const formData = new FormData();
    formData.append('offerID', offerID);
    formData.append('asin', asin);
    formData.append('marketplace', marketplace);
    formData.append('sessionID', sessionID);
    return this.http.post('http://localhost:3002/product.php', formData);
  }
}
